// setup dependencies/modules
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

// server setup
const app = express();
const port = 3001;

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended : true }));


// db connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.w87b5cj.mongodb.net/b217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}.`));