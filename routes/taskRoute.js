// contains all endpoints for our application
const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js")

// [section] : routes
// - responsible for defining or creating endpoints
// - all the business logic is done in the controller

router.get("/viewTasks", (req, res) => {
	// invokes the getAllTasks function from taskController.js
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

		// activity
		router.get("/specificTask/:id", (req, res) => {
	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController =>  res.send(resultFromController));
})

router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskController.UpdateStatus(req.params.id, req.params.status).then(
		resultFromController => res.send(resultFromController));
})

module.exports = router;